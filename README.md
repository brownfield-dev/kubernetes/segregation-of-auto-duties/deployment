# Deployment Project

## AKA: 

* Independent Validation and Verification (IV&V)
* Change Management (CM)
* Transition to Ops

## Purpose  

The primary purpose for multiple projects is that GitLab overpowers the Maintainer role and limits the Developer role such that product owners who aren't allowed to impact production deployments can't operate effectively.  The best work-around as of 13.1-ish is to divide the project into 2 and automate as much as possible. 

The `fast` version is also demonstrated in this group. You can see how the same pattern without the same deployment gates can work as those gates are automated or in a situation where they're not required. 

## Overview 

![overview](docs/overview.jpg)

This ugly diagram shows the workflow and how it traverses 2 projects.  

## This example

This is an empty project that represents a change management workflow that acts as the `CD` part of the `CI/CD` pipeline in a compliance-heavy environment. 

In a setting that Auto DevOps would not work, this project would likely be full of deployment scripts or Dockerfiles for deployable products or Helm charts or Kubernetes configs or Ansible playbooks or Shell scripts.  The point of this project is that it's version controlled along with the creative project. The creative project creates MRs in this project targeting the default branch with updated artifacts.  

This project's pipeline triggers on the creation of the new MR at which point the stage (User Acceptance Test) environment is deployed.  When that test concludes effectively, the merge request is merged into the master branch.  This then pauses and waits for the go-ahead before deploying. An additional manual approval step can be done by one person and the manual job trigger can be assigned to another person.  This creates the maximum number of gates, but at any point they can be removed by changing the jobs to automatically run rather than manual triggers

## The Pipeline

Most of the pipeline happens in `Spring Web App` where the project is compiled, containerized, tested, deployed to a review app, and security scanned.  At some point the creative project has a commit tagged for release and it reaches out to this project. 

For a deeper dive on jobs, check out the [Pipeline information](docs/Pipeline.md) in the docs folder.

A CI job hands off the pipeline and artifacts to a new merge request in this project at which point we do the following: 

1. Get handoff => Checks for the dotenv file called `handoff.env` which has `CI_APPLICATION_` variables for auto deploy
1. Fake validation jobs
1. Stage Approval => Waits for someone to click go and unleashes the pipeline [not present in fast version]
1. Stage Deployment => Uses a mixture of Stage + Review App configurations to deploy a user acceptance environment

If all is well, we do a merge event which triggers a short pipeline

1. Get Handoff => these are the same ones that were operated on previously
1. Production approval => Waits for someone to click go [not present in fast version]
1. Production_manual => Waits for someone to click go, then deploys to production

The gates in this workflow: 

1. Merge request in `Spring Web App` (team lead)
1. Tag in `Application 1` repository (product owner)
1. Stage approval job (UAT / Stage environment owner)
1. Merge request approver and merge event (Security or other parties can weigh in)
1. Production approval job (final approval)
1. Production deployment job (production deployment can be scheduled and executed after approval)

## C is for Continuous

In the above example, the CI pipeline is following continuous themes, building and testing every commit and rolling successful tests into a review app. Since Stage and Production require manual intervention, they are not continuous. It's still called "CD" in this repository because the intention is that by separating it into its own location, the team can iteratively automate the validation steps, implement monitoring, and automate roll-backs if issues arise. 
This is a continuous deliver project that just isn't done yet. 
