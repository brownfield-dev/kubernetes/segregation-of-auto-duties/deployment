# Permissions for the Segregation of Duties

Users access table

| User  | SoAD Group | Spring Web App | Deployment |
| ----- | -----     | -----         | ----- |
| Access Control | Maintainer/Owner | N/A | N/A |
| Application Owner | Developer | Maintainer | N/A |
| Developer or Analyst | Developer | N/A | N/A |
| Change Manager | Developer | N/A | Maintainer |

**NOTE: N/A means the group level access flows down. No need for explicitly modifying it.**

The above table assumes both the web app and deployment project are in the same group. The pipeline allows the MR to be created on a different server or in a different group if desired. This breaks the feedback loop more, but increase isolation of teams if that is the priority. 