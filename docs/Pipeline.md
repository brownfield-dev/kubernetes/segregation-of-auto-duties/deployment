# The Whole Pipeline

This is in the deploy project but it covers both projects pipelines.

## Application / Creative / Spring Web App pipelines

The application pipelines are important for the tight feedback loops for developers. The security scans, quality information, and review success/failure is vital feedback. 

There are multiple types of pipelines that run in this project. The only one that is specific to this workflow is the **Tag release pipeline** below. 

### Commit pipeline

Entirely the same as Auto DevOps. GitLab has docs on what those jobs do and how so feel free to investigate. 

The outcome of successful pipelines is a merge request full of test results, security info, license info, a container tagged with the branch and commit hash, and a running review app.

### Merge-to-master pipeline

Merge to master pipeline is **mostly** the same as Auto DevOps, but without the production deployment. Canary and stage have also been disabled as shown in the job review section below.  Since it uses the `rules:` syntax, I just use the `CANARY_ENABLED` variable to kill all three of them in this project. 

### Tag release pipeline

This is the magical pipeline which does the handoff job. It **could** benefit from removing some of the intermediate jobs. Since it builds a container with the git tag as a docker tag, the build job needs to happen. The test job should run too, just to make sure dependencies didn't change since the last time it ran for a branch or the merge pipeline. 

An important workflow note is that a commit can be tagged on any branch by the person who has permissions to tag it. There's no guarantee that the merge-to-master pipeline will ever have run prior to the tag. 

## Deployment / Compliance / Change Management pipelines

The deployment project (this project) uses Auto DevOps but doesn't include most of the templates. The Deploy template includes the Review, Stage, Canary, Production[_manual], and incremental roll-out jobs that are all specified by rules that limit them by the branch. 

In our case, we want the stage deployment to be dynamic (like Review) so that multiple submitted MRs don't conflict when they start rolling out stage environments.  To support this, a `cm staging` job was created and the regular staging job was killed. 

### Commit pipeline

Gets variables into the dotenv report. 

Runs fake validation jobs. These could be schema checks or linters or even unzipping an artifact and making sure the files inside can be read. 

In the gated deployment application, it has a job to wait for approval before deploying. 

Deploys to a staging application for review, live testing, user acceptance, and to validate the deployment job itself. 

Clean-up job that runs when the branch is deleted. 

### Merge-to-master pipeline

Gets variables into the dotenv report. 

Deploys to the production environment 

# Job Breakdown

## Application / Creative / Spring Web App jobs

### Never job overrides

Since the "Deploy" include file has the review app deployment, it needed to be included. This brings along with it canary, stage, and various production or incremental rollout jobs as well.  To get rid of the ones that would have existed, I created these jobs. 

```yaml
canary:
  rules: 
    - if: '$CANARY_ENABLED'
      when: never

production:
  rules: 
    - if: '$CANARY_ENABLED'
      when: never

production_manual:
  rules: 
    - if: '$CANARY_ENABLED'
      when: never
```

### Handoff jobs

These are only separated so I could run them individually for testing purposes and becase Lab Assistant will take on the job of creating the commit in an upcoming enhancement. 

```yaml
handoff:gated:
  stage: production
  rules:
    - if: '$CI_COMMIT_TAG =~ /^v/'
      when: always
    - when: never
  image: registry.gitlab.com/brownfield-dev/public/lab-assistant:latest
  script:
    - lab-assistant --target-project-id=19978359 --target-user-id=3548980
  needs: 
    - test
```

For the `handoff:gated` and `handoff:fast` jobs, it's using the AutoDevOps `production` stage, even though it's not going to production. This can be modified to a more meaningful name, but since we had lots of unused stages, I reused that one. 

The rules use a `=~` test to see if the tag starts with the letter `v` which is also set as a protected tag pattern `v*` in the repository settings. This means that the job will only be created when a user tags a commit with a tag like `v1` or `very nice commit` or anything that starts with `v`. 

Because that pattern is restricted, it becomes a gate, though because it's in a pipeline, anybody can change this job so it doesn't have that rule applied anymore.  Furthermore, the `project-id` can be changed by anybody to make the job target another project.  The best mitigation for this is to have a bot user account that can only access the project at hand and only as a `developer` so the worst case is a bunch of extra MRs are created. 

The lab assistant application does 3 things. 

1. Create a branch
2. Create a merge request targeting master from that branch
3. Commits a new `handoff.env` with the updated path

## Deployment / Compliance / Change Management pipelines

### Never job overrides

Needed to kill some jobs so they never run. The dependencies brackets are 

```yaml
staging:
  rules:
    - when: never
  dependencies: []

review:
  rules:
    - if: '$CI_COMMIT_TAG || $CI_COMMIT_BRANCH'
      when: never
  dependencies: []

stop_review:
  rules:
    - if: '$CI_COMMIT_TAG || $CI_COMMIT_BRANCH'
      when: never
  dependencies: []

canary:
  rules:
    - if: '$CANARY_ENABLED'
      when: never
  dependencies: []
```

### Commit pipeline

In commit pipelines, the important jobs are: 

1. get artifacts
2. staging approval
3. cm staging
4. cm_stop_staging

The `get artifacts` job uses a text file match the Auto DevOps convention for container naming and makes them override the environment variables for later jobs. 

```yaml
get handoff:
  stage: handoff
  script:
    - echo handoff.env
  artifacts:
    paths:
      - handoff.env
    reports:
      dotenv: handoff.env
```

The `staging approval` job creates a mid-pipeline gate to prevent deployment until someone triggers this job. The protected environment pattern should be `staging/*` to prevent it from deploying inappropriately.

```yaml
staging approval:
  stage: gate1
  script:
    - echo "stage deployment of ${CI_APPLICATION_REPOSITORY}:${CI_APPLICATION_TAG} approved by $GITLAB_USER_LOGIN"
  environment:
    name: staging/$CI_COMMIT_REF_NAME
  dependencies: [ 'get handoff' ]
  allow_failure: false
  rules:
    - if: '$CI_COMMIT_BRANCH == "master"'
      when: never
    - when: manual
```

the `cm staging` job is a copy of the Auto DevOps review job with the environment, stage, dependency, rules, and needs changed. 

The `cm_staging_stop` job below is from Auto DevOps as well but adjusted to match `cm staging`.

```yaml
cm staging: 
  extends: .auto-deploy
  stage: staging
  script:
    - auto-deploy check_kube_domain
    - auto-deploy download_chart
    - auto-deploy ensure_namespace
    - auto-deploy initialize_tiller
    - auto-deploy create_secret
    - auto-deploy deploy 
    - auto-deploy persist_environment_url
  environment:
    name: staging/$CI_COMMIT_REF_NAME
    url: http://$CI_PROJECT_ID-$CI_ENVIRONMENT_SLUG.$KUBE_INGRESS_BASE_DOMAIN
    on_stop: cm_stop_staging
  dependencies: [ 'get handoff' ]
  artifacts:
    paths: [environment_url.txt, tiller.log]
  rules:
    - if: '$CI_KUBERNETES_ACTIVE == null || $CI_KUBERNETES_ACTIVE == ""'
      when: never
    - if: '$CI_COMMIT_BRANCH != "master"'
      when: always
  needs:
    - "staging approval"
    - 'get handoff'

cm_stop_staging:
  extends: .auto-deploy
  stage: cleanup
  variables:
    GIT_STRATEGY: none
  script:
    - auto-deploy initialize_tiller
    - auto-deploy delete
  environment:
    name: staging/$CI_COMMIT_REF_NAME
    action: stop
  dependencies: []
  allow_failure: true
  rules:
    - if: '$CI_KUBERNETES_ACTIVE == null || $CI_KUBERNETES_ACTIVE == ""'
      when: never
    - if: '$CI_COMMIT_BRANCH != "master"'
      when: manual
```

### Merge-to-master pipeline

Same `get artifacts` job as above.

```yaml
get handoff:
  stage: handoff
  script:
    - echo handoff.env
  artifacts:
    paths:
      - handoff.env
    reports:
      dotenv: handoff.env
```

Same kind of approval job as above. 

```yaml
production approval:
  stage: gate2
  script:
    - echo "production release approved by $GITLAB_USER_LOGIN"
  environment: 
    name: production
  allow_failure: false
  rules:
    - if: '$CI_COMMIT_BRANCH == "master"'
      when: manual

production_manual: 
  dependencies: [ 'get handoff' ]
```

Since we have production set to only run on manual deployments, the production_manual job needed to be have the `dependencies` overridden to include the dotenv artifact from `get handoff`

The `get handoff` job creates the variables needed to set the image and tag in the auto-deploy-app for deployment to Kubernetes.
